import { useStore } from 'vuex'

import { MutationTypes } from '../types/mutation-types'

export default function useClose() {
  const store = useStore()

  const closeModal = () => {
    store.commit(MutationTypes.SET_MODAL_DATA, { active: false })
    store.commit(MutationTypes.TOGGLE_MODAL_VISIBILITY, false)
  }

  return { closeModal }
}
