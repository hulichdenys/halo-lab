import { reactive, watch } from 'vue'

import { ValidationSchema, ValidationParams } from '../types/validation-sheme'

const required = (value: string): boolean => value.trim() !== ''
const onlyLetters = (value: string): boolean =>
  /^([a-zа-яёїієґ\s']+|\d+)$/gim.test(value.trim())
const onlyNumbers = (value: string): boolean =>
  /(\+?\d[- ().]*){12}/.test(value.trim())
const minLength = (value: string, length: number) => {
  return value.trim().replace(/[-+ ().]/gim, '').length === length
}

function validation(
  data: ValidationSchema[],
  value: string,
  index: number,
  schema: ValidationSchema[]
) {
  const validationParams: ValidationParams[] = schema[index].params

  // eslint-disable-next-line
  validationParams.forEach((item: any, paramIndex: number) => {
    switch (item.type) {
      case 'required':
        data[index].params[paramIndex].error = !required(value)
        break
      case 'onlyLetters':
        data[index].params[paramIndex].error = !onlyLetters(value)
        break
      case 'onlyNumbers':
        data[index].params[paramIndex].error = !onlyNumbers(value)
        break
      case 'minLength':
        data[index].params[paramIndex].error = !minLength(value, item.value)
        break
    }
  })
}

function hasError(data: ValidationSchema[]) {
  return data.reduce((errorCollection: ValidationParams[], item) => {
    const errors: ValidationParams[] = item.params.filter(
      (param: ValidationParams) => param.error
    )

    errorCollection.push(...errors)

    return errorCollection
  }, []).length
}

function validateAll(data: ValidationSchema[], schema: ValidationSchema[]) {
  for (const index in data) {
    validation(data, data[index].value, Number(index), schema)
  }
}

export default function useValidation(validationSchema: ValidationSchema[]) {
  const { data } = reactive({
    data: validationSchema
  })

  for (const index in data) {
    watch(
      () => data[index].value,
      (newValue) => validation(data, newValue, Number(index), validationSchema)
    )
  }

  const checkValidity = validateAll.bind(null, data, validationSchema)
  const isInvalid = hasError.bind(null, data)

  return {
    data,
    checkValidity,
    isInvalid
  }
}
