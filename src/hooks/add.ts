import { useStore } from 'vuex'

import { Product } from '../types/product-types'
import { Modal } from '../types/modal-types'
import { MutationTypes } from '../types/mutation-types'

export default function useAdd() {
  const store = useStore()

  const createModal = (product: Product, modalType: string) => {
    const modalData: Modal = {
      active: false,
      type: modalType,
      productData: product
    }

    store.commit(MutationTypes.SET_MODAL_DATA, modalData)
    store.commit(MutationTypes.TOGGLE_MODAL_VISIBILITY, true)
  }

  return { createModal }
}
