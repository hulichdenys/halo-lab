import { ActionTree, ActionContext } from 'vuex'
import { State } from './state'
import { Mutations } from './mutations'
import { Product } from '../types/product-types'
import { ActionTypes } from '../types/action-types'
import { MutationTypes } from '../types/mutation-types'

type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>
} & Omit<ActionContext<State, State>, 'commit'>

export interface Actions {
  [ActionTypes.GET_PRODUCTS](
    { commit }: AugmentedActionContext,
    payload: Product[]
  ): void
  // Promise<Product[]>
}

export const actions: ActionTree<State, State> & Actions = {
  async [ActionTypes.GET_PRODUCTS]({ commit }) {
    const response = await fetch(
      'https://run.mocky.io/v3/b7d36eea-0b3f-414a-ba44-711b5f5e528e'
    )
    const data = await response.json()

    commit(MutationTypes.SET_PRODUCTS, data)
  }
}
