import { MutationTree } from 'vuex'
import { State } from './state'

import { MutationTypes } from '../types/mutation-types'
import { Product } from '../types/product-types'
import { Modal } from '../types/modal-types'

export interface Mutations<S = State> {
  [MutationTypes.SET_PRODUCTS](state: S, payload: Product[]): void
  [MutationTypes.SET_MODAL_DATA](state: S, payload: Modal): void
  [MutationTypes.TOGGLE_MODAL_VISIBILITY](state: S, payload: boolean): void
}

export const mutations: MutationTree<State> & Mutations = {
  [MutationTypes.SET_PRODUCTS](state, payload: Product[]) {
    state.products = payload
  },
  [MutationTypes.SET_MODAL_DATA](state, payload: Modal) {
    state.modal = payload
  },
  [MutationTypes.TOGGLE_MODAL_VISIBILITY](state, payload: boolean) {
    state.modal.active = payload
  }
}
