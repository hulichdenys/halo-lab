import { GetterTree } from 'vuex'
import { State } from './state'

import { Product } from '../types/product-types'

export type Getters = {
  cheapProduct(state: State): Product
}

export const getters: GetterTree<State, State> & Getters = {
  cheapProduct: (state) => {
    const sortedProducts = state.products.sort(
      (a, b) => Number(a.price) - Number(b.price)
    )

    return sortedProducts[0]
  }
}
