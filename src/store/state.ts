import { Product } from '../types/product-types'
import { Modal } from '../types/modal-types'

export const state = {
  products: [],
  modal: {
    active: false
  }
}

export interface State {
  products: Product[]
  modal: Modal | { active: boolean }
}
