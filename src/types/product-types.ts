export interface Product {
  name: string
  category: string
  price: string
}

export interface ProductReadOnly {
  readonly name: string
  readonly category: string
  readonly price: string
}
