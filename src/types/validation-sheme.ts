export interface ValidationSchema {
  value: string
  name: string
  params: ValidationParams[]
}

export interface ValidationParams {
  type: ValidationTypes
  value?: number
  error: boolean
  errorMessage: string
}

export type ValidationFields = 'name' | 'number' | string

export type ValidationTypes =
  | 'required'
  | 'onlyLetters'
  | 'onlyNumbers'
  | 'minLength'
