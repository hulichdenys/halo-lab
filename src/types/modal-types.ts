import { Product } from './product-types'

export interface Modal {
  active: boolean
  type?: string
  productData?: Product
  message?: {
    status: string
    title: string
  }
}
